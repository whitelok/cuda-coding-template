/*
cudaMemcpy2D( d_A, // 目的指针
    d_pitch, // 目的pitch
    bmp1,  // 源指针
    sizeof(int)*2,  // 源数据pitch
    sizeof(int)*2,  // 数据拷贝宽度
    2,              // 数据拷贝高度
    cudaMemcpyHostToDevice);//从CPU拷贝二维数组到GPU上
*/
#include<stdio.h>
#include<cuda.h>

__global__ void matmul2D(float *data, int w, int h, size_t pitch){
    int blockId =  gridDim.x * gridDim.y * blockIdx.z + gridDim.y * blockIdx.x + blockIdx.y;
    int innerBlockId = blockDim.x * blockDim.y * threadIdx.z + blockDim.y * threadIdx.x + threadIdx.y;
    int threadId = blockId * blockDim.x * blockDim.y * blockDim.z + innerBlockId;
    
    __shared__ float shareData[64];
    
    if (blockId < h){
        printf("%d %d\n", innerBlockId, blockId);
        
        float *row = (float *)((char*)data + (blockId * pitch));
        shareData[innerBlockId] = row[innerBlockId];
        __syncthreads();

        shareData[innerBlockId] += 1;
        __syncthreads();

        row[innerBlockId] = shareData[innerBlockId];
        __syncthreads();
    }
}


int main(void){
    float *data_h, *data_d;
    
    int w = 64, h = 64;
    
    cudaMallocHost(&data_h, w * h * sizeof(float));
    
    for(int i = 0; i < h; i++){
        for(int j =0; j < w; j++){
            data_h[i * w + j] = (float)(i * w + j);
            //data_h[i * w + j] = 1;
        }
    } 

    for(int i = h-2; i < h; i++){
        for(int j =0; j < w; j++){
            printf("%f ", data_h[i * w + j]);
        }
        printf("\n");
    }
    printf("\n");

    cudaStream_t stream;
    cudaStreamCreate(&stream);
    
    size_t pitch;
    cudaMallocPitch(&data_d, &pitch, w * sizeof(float), h);
    
    cudaMemcpy2DAsync(data_d, pitch, data_h, w * sizeof(float), w * sizeof(float), h, cudaMemcpyHostToDevice, stream);
    matmul2D<<<128, 64, 0, stream>>>(data_d, w, h, pitch);
    cudaMemcpy2DAsync(data_h, w * sizeof(float), data_d, pitch, w * sizeof(float), h, cudaMemcpyHostToDevice, stream);    
    
    cudaStreamSynchronize(stream);
    
    for(int i = 0; i < h; i++){
        for(int j = 0; j < w; j++){
            printf("%f ", data_h[i * w + j]);
        }
        printf("\n");
    }
    printf("\n");
    
    cudaFree(data_d);
    cudaFreeHost(data_h);
    
    cudaStreamDestroy(stream);
}