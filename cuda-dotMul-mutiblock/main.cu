// 使用CUDA实现两个向量A和B的内积（点积）， 假设向量A和B都包括2048个元素，数据类型是32-bit floating point
#include<stdio.h>

#define LEN 2048

__global__ void dotMul(const float *a, const float *b, float *c){
    __shared__ float buffer1[512];
    __shared__ float buffer2[512];
    
    int blockId = gridDim.x * gridDim.y * blockIdx.z + gridDim.y * blockIdx.x + blockIdx.y;
    int innerBlockId = blockDim.x * blockDim.y * threadIdx.z + blockDim.y * threadIdx.x + threadIdx.y;
    int threadId = blockId * blockDim.x * blockDim.y * blockDim.z + innerBlockId;
        
    buffer1[innerBlockId] = a[threadId];
    buffer2[innerBlockId] = b[threadId];
    __syncthreads();
    
    buffer1[innerBlockId] *= buffer2[innerBlockId];
    __syncthreads();
    
    for(int s = 512 / 2; s > 0; s>>=1){
        if(innerBlockId < s){
            buffer1[innerBlockId] += buffer1[innerBlockId + s];  
        }
        __syncthreads();
    }
    
    c[blockId] = buffer1[0];
    __syncthreads();
}


int main(void){

    //stream
    
    //cudaMallocPitch
    
    //texture 

    float *a_h, *b_h, *c_h;
    float *a_d, *b_d, *c_d;
    
    dim3 gridDim(4,1,1);
    dim3 blockDim(512,1,1);

    size_t v_size = LEN * sizeof(float);
    cudaMallocHost(&a_h, v_size);
    cudaMallocHost(&b_h, v_size);
    cudaMallocHost(&c_h, 4 * sizeof(float));
    
    for(int i = 0; i < 2048; i++){
        a_h[i] = 1;
        b_h[i] = 2;
    }
    
    cudaMalloc(&a_d, v_size);
    cudaMalloc(&b_d, v_size);
    cudaMalloc(&c_d, 4 * sizeof(float));
    
    cudaMemcpy(a_d, a_h, v_size, cudaMemcpyHostToDevice);
    cudaMemcpy(b_d, b_h, v_size, cudaMemcpyHostToDevice);
    dotMul<<<gridDim, blockDim>>>(a_d, b_d, c_d);
    cudaMemcpy(c_h, c_d, 4 * sizeof(float), cudaMemcpyDeviceToHost);
    
    float sum = c_h[0] + c_h[1] + c_h[2] + c_h[3];
    printf("result is : %f\n", sum); 
    
    cudaFreeHost(a_h);
    cudaFreeHost(b_h);
    cudaFreeHost(c_h);
    cudaFree(a_d);
    cudaFree(b_d);
    cudaFree(c_d);
}