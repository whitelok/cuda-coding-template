#include <stdio.h>
#include <cuda.h>

//u(i)= u(i)^2 + u(i-1)
//Static
__global__ void example(float* u) {
    int i=threadIdx.x;
    __shared__ int tmp[4];
    tmp[i]=u[i];
    u[i]=tmp[i]*tmp[i]+tmp[3-i];
}

int main() {
    float hostU[4] = {1, 2, 3, 4};
    float* devU;
    size_t size = sizeof(float)*4;
    cudaMalloc(&devU, size);
    cudaMemcpy(devU, hostU, size, cudaMemcpyHostToDevice);
    example<<<1,4>>>(devU);
    cudaMemcpy(hostU, devU, size, cudaMemcpyDeviceToHost);

    for(int i = 0; i < 4; i++){
        printf("%f\n", hostU[i]);
    }
    cudaFree(devU);
    return 0;
}
