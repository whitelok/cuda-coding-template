#include <stdio.h>

/***
输入一张彩色图，转灰度图。

提示：
in CUDA, each pixel is represented in struct uchar4:
unsigned char r,b,g,w;//w: alpha channel, represent the transparent information
灰度转换公式：I = .299*r+.587*g+.114b（给rbg设置不同weight是人眼对不同通道的敏感度不同所致）
***/

typedef struct{
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char w;
} Point;

#define IMAGE_SIZE 64*64

/*
__global__ void exercise2(const Point *input, float *output){
    const int blockId = gridDim.x * gridDim.y * blockIdx.z + gridDim.y * blockIdx.x + blockIdx.y;
    const int innerBlockId = blockDim.x * blockDim.y * threadIdx.z + blockDim.y * threadIdx.x + threadIdx.y;
    const int threadId = blockId * gridDim.x * gridDim.y * gridDim.z + innerBlockId;
    
    extern __shared__ unsigned char data[];
    Point *imagePointer = (Point *)data;
    float *outputDataPointer = (float *)&imagePointer[128];

    imagePointer[innerBlockId] = input[threadId];
    __syncthreads();
    
    printf("%d \n", threadId);
    
    outputDataPointer[innerBlockId] = (0.299 * imagePointer[innerBlockId].r + 0.587 * imagePointer[innerBlockId].g + 0.114 * imagePointer[innerBlockId].b);
    __syncthreads();
    
    output[threadId] = outputDataPointer[innerBlockId];
    __syncthreads();
    //printf("%f \n", outputDataPointer[innerBlockId]);
}
*/

__global__ void exercise2(const Point *input, float *output){
    const int blockId = gridDim.x * gridDim.y * blockIdx.z + gridDim.y * blockIdx.x + blockIdx.y;
    const int innerBlockId = blockDim.x * blockDim.y * threadIdx.z + blockDim.y * threadIdx.x + threadIdx.y;
    const int threadId = blockId * gridDim.x * gridDim.y * gridDim.z + innerBlockId;
    
    extern __shared__ Point data[];

    data[innerBlockId] = input[threadId];
    __syncthreads();
    
    data[innerBlockId].w = (0.299 * data[innerBlockId].r + 0.587 * data[innerBlockId].g + 0.114 * data[innerBlockId].b);
    __syncthreads();

    output[threadId] = (float)data[innerBlockId].w;
    __syncthreads();
    //printf("%f \n", outputDataPointer[innerBlockId]);
}

int main(void){
    Point *input_h, *input_d;
    float *output_h, *output_d;
    size_t imageMemSize = IMAGE_SIZE * sizeof(Point);
    cudaMallocHost(&input_h, imageMemSize);
    cudaMallocHost(&output_h, IMAGE_SIZE * sizeof(float));
    cudaMalloc(&input_d, imageMemSize);
    cudaMalloc(&output_d, IMAGE_SIZE * sizeof(float));
    
    for(int i = 0; i < IMAGE_SIZE; i++){
        input_h[i].r = i * 103 % 255;
        input_h[i].g = i * 104 % 255;
        input_h[i].b = i * 97 % 255;
        input_h[i].w = i * 4 % 255;
    }
    
    for(int i = 0; i < IMAGE_SIZE; i++){
        //printf("%d\n", input_h[i].r);
    }
    
    // cpy from host
    cudaMemcpy(input_d, input_h, imageMemSize, cudaMemcpyHostToDevice);
    
    exercise2<<<8, 128, 128 * sizeof(Point) + 12899999999999999999999 * sizeof(float)>>>(input_d, output_d);
    
    // cpy from device
    cudaMemcpy(output_h, output_d, IMAGE_SIZE * sizeof(float), cudaMemcpyDeviceToHost);
    
    for(int i = 0; i < IMAGE_SIZE; i++){
        printf("%f ", output_h[i]);
    }
    printf("\n");
    
    // free 
    cudaFree(input_d);
    cudaFree(output_d);
    cudaFree(input_h);
    cudaFree(output_h);
}