#include <stdio.h>
#include <cuda.h>

#define BLOCKS 4
#define THREADS 8

__device__ void exchange(float &a, float &b){
    printf("changing: %f %f\n", a, b);
    float tmp;
    tmp = a;
    a = b;
    b = tmp;
}

__global__ void oddEvenSort(float* data) {
    __shared__ float tmp[THREADS*BLOCKS];
    
    int blockId = gridDim.x * gridDim.y * blockIdx.z + gridDim.y * blockIdx.x + blockIdx.y;
    int innerBlockId = blockDim.x * blockDim.y * threadIdx.z + blockDim.y * threadIdx.x + threadIdx.y;
    int threadId = blockId * blockDim.x * blockDim.y * blockDim.z + innerBlockId;
    
    tmp[threadId] = data[threadId];
    __syncthreads();
    
    for(int i = 0; i < BLOCKS*THREADS; i++){
        if(i % 2 == 0){
            if(threadId % 2 == 0){
                if(threadId + 1 < BLOCKS*THREADS && tmp[threadId] > tmp[threadId + 1])
                    exchange(tmp[threadId], tmp[threadId + 1]);
            }else{
                if(threadId - 1 >= 0 && tmp[threadId - 1] > tmp[threadId])
                    exchange(tmp[threadId - 1], tmp[threadId]);
            }
        }else{
            if(threadId % 2 == 0){
                if(threadId - 1 >= 0 && tmp[threadId - 1] > tmp[threadId])
                    exchange(tmp[threadId - 1], tmp[threadId]);
            }else{
                if(threadId + 1 < BLOCKS*THREADS && tmp[threadId] > tmp[threadId + 1])
                    exchange(tmp[threadId], tmp[threadId + 1]);
            }
        }
        __syncthreads();
    }
    
    for(int i = 0; i < BLOCKS * THREADS; i++){
        printf("%f\n", tmp[i]);
    }
    
    data[innerBlockId] = tmp[innerBlockId];
    __syncthreads();
}

int main() {
    float *data_h, *data_d;
    
    cudaMallocHost(&data_h, BLOCKS * THREADS * sizeof(float));
    cudaMalloc(&data_d, BLOCKS * THREADS * sizeof(float));
    
    for(int i = 0; i < BLOCKS * THREADS; i++){
        data_h[i] = BLOCKS * THREADS - i;
    }
    
    cudaMemcpy(data_d, data_h, BLOCKS * THREADS * sizeof(float), cudaMemcpyHostToDevice);
    oddEvenSort<<<1, BLOCKS*THREADS>>>(data_d);
    cudaMemcpy(data_h, data_d, BLOCKS * THREADS * sizeof(float), cudaMemcpyDeviceToHost);
    
    for(int i = 0; i < BLOCKS * THREADS; i++){
        printf("%f\n", data_h[i]);
    }
    cudaFree(data_d);
    cudaFree(data_h);
}
