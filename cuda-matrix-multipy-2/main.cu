#include<stdio.h>

/*
validate by python:
import numpy as np
a = np.array([i for i in range(1, 21)]).reshape([-1,5])
b = np.array([i for i in range(1, 16)]).reshape([5, 3]) + 1
np.matmul(a, b)
*/

__global__ void matrixMultiply(float *a, float *b, const int m, const int n, const int k, float *c){
    // len: (m*n + n*k + m*k)
    extern __shared__ float total[];
    
    int blockId = blockIdx.x + gridDim.x * blockIdx.y + gridDim.x * gridDim.y * blockIdx.z;
    int innerBlockId = threadIdx.z * blockDim.x * blockDim.y + blockDim.x * threadIdx.y + threadIdx.x;
    int threadId = blockId * (blockDim.x * blockDim.y * blockDim.z) + innerBlockId;
    
    float *tmp_a, *tmp_b, *tmp_c;
    
    tmp_a = total;
    tmp_b = total + m * n;
    tmp_c = total + m * n + n * k;
    
    tmp_a[threadIdx.x * n + threadIdx.y] = a[threadIdx.x * n + threadIdx.y];
    tmp_b[threadIdx.y * k + threadIdx.z] = b[threadIdx.y * k + threadIdx.z];
    __syncthreads();
    
    atomicAdd(&tmp_c[threadIdx.x * k + threadIdx.z], tmp_a[threadIdx.x * n + threadIdx.y] * tmp_b[threadIdx.y * k + threadIdx.z]);
    __syncthreads();
    
    a[threadIdx.x * n + threadIdx.y] = tmp_a[threadIdx.x * n + threadIdx.y];
    b[threadIdx.y * k + threadIdx.z] = tmp_b[threadIdx.y * k + threadIdx.z];
    c[threadIdx.x * k + threadIdx.z] = tmp_c[threadIdx.x * k + threadIdx.z];
    __syncthreads();
}

int main(void){
    // (4 * 5) * (5 * 3)
    float *a_h, *b_h, *c_h;
    float *a_d, *b_d, *c_d;
    int m = 4, n = 5, k = 3;

    // (4 * 5)
    dim3 gridsize(4, 3, 1);
    dim3 blocsize(4, 5, 3);
    
    size_t size = sizeof(float) * 20;
    
    cudaMallocHost(&a_h, sizeof(float) * 20);
    cudaMallocHost(&b_h, sizeof(float) * 15);
    cudaMallocHost(&c_h, sizeof(float) * 12);
    
    for(int i = 0; i < 20; i++){
        a_h[i] = i + 1;
    }
    for(int i = 0; i < 15; i++){
        b_h[i] = i + 2;
    }
    
    cudaMalloc(&a_d, sizeof(float) * 20);
    cudaMalloc(&b_d, sizeof(float) * 15);
    cudaMalloc(&c_d, sizeof(float) * 12);
    
    cudaMemcpy(a_d, a_h, sizeof(float) * 20, cudaMemcpyHostToDevice);
    cudaMemcpy(b_d, b_h, sizeof(float) * 15, cudaMemcpyHostToDevice);
    matrixMultiply<<<gridsize, blocsize, (m*n + n*k + m*k) * sizeof(float)>>>(a_d, b_d, m, n, k, c_d);
    cudaMemcpy(c_h, c_d, sizeof(float) * 12, cudaMemcpyDeviceToHost);
    cudaMemcpy(a_h, a_d, sizeof(float) * 20, cudaMemcpyDeviceToHost);
    cudaMemcpy(b_h, b_d, sizeof(float) * 15, cudaMemcpyDeviceToHost);
    
    for(int i = 0; i < 20; i++){
        printf("%f ", a_h[i]);
    }
    
    printf("\n");
    
    for(int i = 0; i < 15; i++){
        printf("%f ", a_h[i]);
    }    
    
    printf("\n");
    
    for(int i = 0; i < 12; i++){
        printf("%f ", c_h[i]);
    }
    
    cudaFree(a_h);
    cudaFree(b_h);
    cudaFree(c_h);
    cudaFree(a_d);
    cudaFree(b_d);
    cudaFree(c_d);
}