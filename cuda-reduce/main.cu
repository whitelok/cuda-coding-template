#include<stdio.h>

__global__ void vector_add_3d(const int *a, const int *b, int *c){
    int blockId = blockIdx.x + gridDim.x * blockIdx.y + gridDim.x * gridDim.y * blockIdx.z;
    int innerBlockId = threadIdx.z * blockDim.x * blockDim.y + blockDim.x * threadIdx.y + threadIdx.x;
    int threadId = blockId * (blockDim.x * blockDim.y * blockDim.z) + innerBlockId;
    
    __shared__ int s_data[128];
    
    if(threadId < 1024){
        s_data[innerBlockId] = a[threadId] + b[threadId];
        __syncthreads();
        
        for(int s = 128 / 2; s > 0; s>>=1){
            if(innerBlockId < s){
                s_data[innerBlockId] += s_data[innerBlockId + s];
            }
            __syncthreads();
        }
    }

    c[threadId] = s_data[innerBlockId];
    __syncthreads();
}

int main(void){
    int *a_h, *b_h, *c_h, *d_h;
    int *a_d, *b_d, *c_d, *d_d;
    
    dim3 gridsize(2, 2, 2);
    dim3 blocsize(4, 2, 16);
    
    size_t size = sizeof(int) * 1024;
    
    cudaMallocHost(&a_h, size);
    cudaMallocHost(&b_h, size);
    cudaMallocHost(&c_h, size);
    cudaMallocHost(&d_h, size);
    
    cudaMallocHost(&d_d, 64 * sizeof(int));
    cudaMemset(d_d, 0, 64 * sizeof(int));
    
    for(int i = 0; i < 1024; i++){
        a_h[i] = i;
        b_h[i] = i;
    }
    
    cudaMalloc(&a_d, size);
    cudaMalloc(&b_d, size);
    cudaMalloc(&c_d, size);
    
    cudaMemcpy(a_d, a_h, size, cudaMemcpyHostToDevice);
    cudaMemcpy(b_d, b_h, size, cudaMemcpyHostToDevice);
    vector_add_3d<<<gridsize, blocsize>>>(a_d, b_d, c_d);
    cudaMemcpy(c_h, c_d, size, cudaMemcpyDeviceToHost);
    
    cudaDeviceSynchronize();
    
    int sum = 0;
    
    for(int i = 0; i < 1024; i+=128){
        printf("%d\n", c_h[i]);
    }
    
    for(int i = 0; i < 1024; i+=128){
        //sum += c_h[i];
    }
    
    //printf("%d\n", sum);
    
    cudaFree(a_h);
    cudaFree(b_h);
    cudaFree(c_h);
    cudaFree(a_d);
    cudaFree(b_d);
    cudaFree(c_d);
    cudaFree(d_d);
    
}