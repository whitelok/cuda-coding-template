#include<stdio.h>

using namespace std;

typedef struct __align__(8) Point{
    int x;
    int y;
}Point;

__global__ void matrix_mul(const float *a, const Point aP, const float *b, const Point bP, float *c, const Point cP){
    int blockId = gridDim.x * gridDim.y * blockIdx.z + gridDim.x * blockIdx.y + blockIdx.x;
    
    int innerBlockId = blockDim.x * blockDim.y * threadIdx.z + blockDim.x * threadIdx.y + threadIdx.x;
    
    int threadId = blockId * blockDim.x * blockDim.y * blockDim.z + innerBlockId;
    
    if(threadId  < (cP.x * cP.y)){
        int row = threadId / cP.y;
        int col = threadId % cP.y;
        
        float sum = 0;
        for(int i = 0; i < bP.x; i++){
            //printf("(%d %d) * (%d %d)\n", row, i, i, row);
            sum += a[row * aP.y + i] * b[i * bP.y + col];
            if(threadId == 1)
            printf("(%d %d) * (%d %d) + ", row, i, i, row);
        }
        printf("\n(%d %d)\n", row, col);
        c[row * cP.y + col] = sum;
        __syncthreads();
        
    }
}

int main( void ) {

    dim3 gridDim(4, 5);
    dim3 blockDim(2, 3, 4);
    
    Point a_p = {4, 5}, b_p = {5, 3}, c_p = {4, 3};
    
    float *a_h, *b_h, *c_h;
    float *a_d, *b_d, *c_d;
    
    cudaMallocHost(&a_h, a_p.x * a_p.y * sizeof(float));
    cudaMallocHost(&b_h, b_p.x * b_p.y * sizeof(float));
    cudaMallocHost(&c_h, c_p.x * c_p.y * sizeof(float));
    
    cudaMalloc(&a_d, a_p.x * a_p.y * sizeof(float));
    cudaMalloc(&b_d, b_p.x * b_p.y * sizeof(float));
    cudaMalloc(&c_d, c_p.x * c_p.y * sizeof(float));
    
    cudaMemset(c_h, 0, 4 * 3 * sizeof(float));
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 5; j++){
            a_h[i * 5 + j] = i * 5 + j + 1;
        }
    }
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 5; j++){
            printf("%f ", a_h[i * a_p.y + j]);
        }
        printf("\n");
    }
    printf("\n================\n");
    
    for(int i = 0; i < 5; i++){
        for(int j = 0; j < 3; j++){
            b_h[i * 3 + j] = i * 3 + j + 1;
        }
    }
    for(int i = 0; i < 5; i++){
        for(int j = 0; j < 3; j++){
            printf("%f ", b_h[i * b_p.y + j]);
        }
        printf("\n");
    }
    printf("\n================\n");
    
    cudaMemcpy(a_d, a_h, a_p.x * a_p.y * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(b_d, b_h, b_p.x * b_p.y * sizeof(float), cudaMemcpyHostToDevice);
    
    matrix_mul<<<gridDim, blockDim>>>(a_d, a_p, b_d, b_p, c_d, c_p);
    
    cudaMemcpy(c_h, c_d, c_p.x * c_p.y * sizeof(float), cudaMemcpyDeviceToHost);
    
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 3; j++){
            printf("%f ", c_h[i * c_p.y + j]);
        }
        printf("\n");
    }
    
    cudaFree(a_h);
    cudaFree(b_h);
    cudaFree(c_h);
    cudaFree(a_d);
    cudaFree(b_d);
    cudaFree(c_d);
}
