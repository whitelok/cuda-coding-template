#include<iostream>
#include<cuda.h>
#include<stdio.h>

using namespace std;

__global__ void getMax(float *data){
    const unsigned int blockId = gridDim.x * gridDim.y * blockIdx.z + gridDim.y * blockIdx.x + blockIdx.y;
    const unsigned int innerBlockId = blockDim.x * blockDim.y * threadIdx.z + blockDim.y * threadIdx.x + threadIdx.y;
    const unsigned int threadId = blockId * (blockDim.x * blockDim.y * blockDim.z) + innerBlockId;
    
    if(threadId < 1024){
        __shared__ float tmpData[512];
        tmpData[innerBlockId] = data[blockId * 512 + innerBlockId];
        __syncthreads();
        for(int i = 1; i < 512; i *= 2){
            if(innerBlockId % (i * 2) == 0 && (innerBlockId + i) < 1024){
                if(tmpData[innerBlockId] < tmpData[innerBlockId + i]){
                    float tmp;
                    tmp = tmpData[innerBlockId];
                    tmpData[innerBlockId] = tmpData[innerBlockId + i];
                    tmpData[innerBlockId + i] = tmp;
                }
            }
            __syncthreads();
        }
        
        data[blockId * 512 + innerBlockId] = tmpData[innerBlockId];
        __syncthreads();
    }
}

int main( void ) {
    float *data_h;
    float *data_d;
    
    dim3 gridDim(4,4,4);
    dim3 blockDim(8,8,8);
    
    cudaMallocHost(&data_h, 1024 * sizeof(float));
    cudaMalloc(&data_d, 1024 * sizeof(float));
    
    for(int i = 0; i < 1024; i++){
        //data_h[i] = (float) rand() / RAND_MAX + (float) rand() / (RAND_MAX * RAND_MAX);
        data_h[i] = i + 1;
    }
    
    for(int i = 0; i < 1024; i++){
        //cout << data_h[i] << endl;
    }

    cudaMemcpy(data_d, data_h, 1024 * sizeof(float), cudaMemcpyHostToDevice);
    getMax<<<gridDim, blockDim>>>(data_d);
    cudaThreadSynchronize();
    cudaMemcpy(data_h, data_d, 1024 * sizeof(float), cudaMemcpyDeviceToHost);
    
    for(int i = 0; i < 1024; i++){
        cout << data_h[i] << endl;
    }
    
    //cout << (data_h[0] > data_h[512]? data_h[0]:data_h[512])  << endl;
    
    cudaFree(data_d);
    cudaFree(data_h);
}
