#include <stdio.h>  

// should be kernel
__device__ void matMul(const float *src, const float *dst, int resultX, int resultY){
    for(int i = 0; i < resultX; i++){
        for(int j = 0; j < resultY; j++){
            src[i * resultX + j] * dst[i + resultY * j];
        }
    }
}

__global__ void conv_nopadding(const float *data, const float *kernel, int kernelH, int kernelW, float *result, const int resultH, const int resultW)  {  
    const int blockId = gridDim.x * gridDim.y * blockIdx.z + gridDim.y * blockIdx.x + blockIdx.y;
    const int innerBlockId = blockDim.x * blockDim.y * threadIdx.z + blockDim.y * threadIdx.x + threadIdx.y;
    const int threadId = blockId * (blockDim.x * blockDim.y * blockDim.z) + innerBlockId;
    
    extern __shared__ float tmpData[];
    
    float tmpResult;
    
    for(int i = 0; i < kernelH; i++){
        for(int j = 0; j < kernelW; j++){
            tmpData[i * kernelW + j] = data[(blockId + i) * 64 + innerBlockId + j];
            tmpData[kernelW * kernelH + i * kernelW + j] = kernel[i * kernelW + j];
        }
    }
    __syncthreads();

    
    for(int row = 0; row < kernelH; row++){
        for(int col = 0; col < kernelW; col++){
            for(int index = 0; index < kernelW; index++){
                tmpResult += tmpData[row * kernelW + index] * tmpData[kernelW * kernelH + index * kernelW + col];
            }
        }
    }
    
    result[blockId * 64 + innerBlockId] = tmpResult;
    __syncthreads();
}  

int main(){  
    int imgH = 64, imgW = 64;
    int kernelH = 2, kernelW = 2;
    int resultH = (imgH - kernelH) + 1, resultW = (imgW - kernelW) + 1;
    
    float *data_h, *kernel_h, *result_h;
    float *data_d, *kernel_d, *result_d;
    
    cudaMallocHost(&data_h, imgH * imgW * sizeof(float));
    cudaMallocHost(&kernel_h, kernelH * kernelW * sizeof(float));
    cudaMallocHost(&result_h, resultH * resultW * sizeof(float));
    
    for(int i = 0; i < imgH; i++){
        for(int j = 0; j < imgW; j++){
            data_h[i * imgW + j] = i * imgW + j;
        }
    }
    
    for(int i = 0; i < kernelH; i++){
        for(int j = 0; j < kernelW; j++){
            kernel_h[i * kernelW + j] = i * kernelW + j;
        }
    }
    
    cudaMalloc(&data_d, imgH * imgW * sizeof(float));
    cudaMalloc(&kernel_d, imgH * imgW * sizeof(float));
    cudaMalloc(&result_d, resultH * resultW * sizeof(float));
    
    cudaMemcpy(data_d, data_h, imgH * imgW * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(kernel_d, kernel_h, kernelH * kernelW * sizeof(float), cudaMemcpyHostToDevice);
    conv_nopadding<<<resultH, resultW>>>(data_d, kernel_d, kernelH, kernelW, result_d, resultH, resultW);
    cudaMemcpy(result_h, result_d, resultH * resultW * sizeof(float), cudaMemcpyDeviceToHost);
    
    for(int i = 0; i < resultH; i++){
        for(int j = 0; j < resultW; j++){
            printf("%f ", result_h[i * resultW + j]);
        }
        printf("\n");
    }
    
    cudaFree(data_d);
    cudaFree(kernel_d);
    cudaFree(data_d);
    cudaFreeHost(data_h);
    cudaFreeHost(kernel_h);
    cudaFreeHost(data_h);
}  