/***
input: float array [0, 1, 2, ... 63]

output: float array [0^2, 1^2, 2^2, ... 63^2]
***/

#include<stdio.h>

#define BLOCK_SIZE 32
#define DATA_LENGTH 64

extern __shared__ int data[];

__global__ void exercise1(const int *input, int *output){
    const unsigned int blockId = gridDim.x * gridDim.y * blockIdx.z + gridDim.y * blockIdx.x + blockIdx.y;
    const unsigned int innerBlockId = blockDim.x * blockDim.y * threadIdx.z + blockDim.y * threadIdx.x + threadIdx.y;
    const unsigned int threadId = blockId * gridDim.x * gridDim.y * gridDim.z + innerBlockId;
    
    //printf("%d\n", innerBlockId);
    
    data[innerBlockId] = input[threadId];
    __syncthreads();
    
    data[innerBlockId] = data[innerBlockId] * data[innerBlockId];
    __syncthreads();
    
    output[threadId] = data[innerBlockId];
 }

int main(void){
    int *input_h, *output_h, *input_d, *output_d;
    dim3 gridDim(DATA_LENGTH / BLOCK_SIZE, 0, 0);
    dim3 blockDim(BLOCK_SIZE, 0, 0);
    
    // init host
    cudaMallocHost(&input_h, DATA_LENGTH * sizeof(int));
    cudaMallocHost(&output_h, DATA_LENGTH * sizeof(int));
    cudaMalloc(&input_d, DATA_LENGTH * sizeof(int));
    cudaMalloc(&output_d, DATA_LENGTH * sizeof(int));    
    for(int i = 0; i < DATA_LENGTH; i++){
        input_h[i] = i;
    }
    
    // cpy from host
    cudaMemcpy(input_d, input_h, DATA_LENGTH * sizeof(int), cudaMemcpyHostToDevice);
    
    exercise1<<<DATA_LENGTH / BLOCK_SIZE, DATA_LENGTH, BLOCK_SIZE * sizeof(int)>>>(input_d, output_d);
    
    // cpy from device
    cudaMemcpy(output_h, output_d, DATA_LENGTH * sizeof(int), cudaMemcpyDeviceToHost);
    
    for(int i = 0; i < 64; i++){
        printf("%d\n", output_h[i]);
    }
    
    // free 
    cudaFree(input_d);
    cudaFree(output_d);
    cudaFree(input_h);
    cudaFree(output_h);
}
