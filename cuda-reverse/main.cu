#include <stdio.h>

__device__ void swap(float &a, float &b){
    float tmp;
    tmp = a;
    a = b;
    b = tmp;
}

__global__ void reverse(float *array) {
    int blockId = blockIdx.x + gridDim.x * blockIdx.y + gridDim.x * gridDim.y * blockIdx.z;
    int innerBlockId = threadIdx.z * blockDim.x * blockDim.y + blockDim.x * threadIdx.y + threadIdx.x;
    int threadId = blockId * (blockDim.x * blockDim.y * blockDim.z) + innerBlockId;
    
    __shared__ float s_data[1024];
    
    s_data[threadId] = array[threadId];
    __syncthreads();
    
    swap(s_data[threadId], s_data[1024 - threadId - 1]);
    __syncthreads();
    
    array[threadId] = s_data[threadId];
    __syncthreads();
}

int main()
{   
    float *h_arr, *d_arr;
    (cudaMallocHost(&h_arr, 1024 * sizeof(float)));
    (cudaMalloc(&d_arr, 1024 * sizeof(float)));
    
    for(int i = 0; i < 1024; i++){
        h_arr[i] = i;
    }
    
    (cudaMemcpy(d_arr, h_arr, sizeof(float) * 1024, cudaMemcpyHostToDevice));
    reverse<<<1, 1024>>>(d_arr);
    (cudaMemcpy(h_arr, d_arr, sizeof(float) * 1024, cudaMemcpyDeviceToHost));

    for(int i = 0; i < 1024; i++){
        printf("%f ", h_arr[i]);
    }

    (cudaFree(d_arr));
    (cudaFreeHost(h_arr));
}
